import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrevisionnelComponent } from './previsionnel.component';

describe('PrevisionnelComponent', () => {
  let component: PrevisionnelComponent;
  let fixture: ComponentFixture<PrevisionnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrevisionnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrevisionnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
