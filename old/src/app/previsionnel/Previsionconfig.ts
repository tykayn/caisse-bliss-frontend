
export const PrevisionConfig = {
  initialLoadingDone: false,
  loading: false,
  lines: 24,
  debounceTime: 300, // miliseconds to wait before updating model and saving changes
  disponibility: 5000,
  averageMonthlyEarnings: 600,
  warningThershold: 2000,
  showDelays: false,
  showRepeats: false,
  monthsBeforeNoMoney: null,
};
