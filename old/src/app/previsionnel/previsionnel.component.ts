import {Component, OnInit} from '@angular/core';
import {PrevisionConfig} from './Previsionconfig';
import {PrevisionExampleExpenses} from './PrevisionExampleExpenses';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {T} from '@angular/core/src/render3';

@Component({
  selector: 'app-previsionnel',
  templateUrl: './previsionnel.component.html',
  styleUrls: ['./previsionnel.component.css']
})
export class PrevisionnelComponent implements OnInit {


  exampleExpenses: any;

  PrevisionExampleExpenses: any = PrevisionExampleExpenses;

  expenses: any = [];
  previsionTable: any = [];
  config: any = PrevisionConfig;
  graphPointsPrevision: any = [];

  constructor(private http: HttpClient) {

  }

  ngOnInit() {
    this.init();
  }


  /**
   * sum of all monthly expenses, ignoring delay
   * @returns {number}
   */
  sumMonthlyExpenses(): number {
    let sum = 0;
    this.expenses.forEach((elem) => {
      if (elem.enabled) {
        sum += elem.amount;
      }
    });
    return sum;
  }


  calculatePrevisionTable() {
    const turns = this.config.lines;
    const monthly = this.sumMonthlyExpenses();
    let available = this.config.disponibility;
    const previsionTable = [];
    let changedNoMoneyConfig = false;
    this.config.monthsBeforeNoMoney = null;
    for (let i = 0; i <= turns; i++) {
      // TODO take in account delays in expenses
      available = available - monthly + this.config.averageMonthlyEarnings;
      const newLine = {
        expense: monthly,
        available: available,
      };

      if (available <= 0 && !changedNoMoneyConfig) {
        this.config.monthsBeforeNoMoney = i;
        changedNoMoneyConfig = true;
      }
      previsionTable.push(newLine);
    }
    this.previsionTable = previsionTable;
    this.makeGraphPointsOfPrevisionTable(previsionTable);
    return previsionTable;
  }


  makeGraphPointsOfPrevisionTable(previsionTable) {

    this.graphPointsPrevision = [];

    for (let i = 0; i < previsionTable.length; i++) {
      this.graphPointsPrevision.push({
        label: previsionTable[i].available + ' euros restants dans ' + i + ' mois',
        y: previsionTable[i].available,
        x: i,
      });
    }

  }

  updateconf(rep) {
    // update view calculs
    this.calculatePrevisionTable();
    this.updateCanevas();
    // flags
    this.config.loading = false;
    this.config.initialLoadingDone = true;
    this.config.disponibility = rep.data.disponibility;
    this.config.averageMonthlyEarnings = rep.data.averageMonthlyEarnings;
    // default data when user has nothing saved
    console.log('rep.data.expenses.length', rep.data.expenses.length);
    if (!rep.data.expenses.length) {
      this.expenses = PrevisionExampleExpenses;
    } else {
      this.expenses = rep.data.expenses;
    }
  }

// http related calls
  fetchExpenses() {
    console.log('fetch expenses...');
    this.config.loading = true;

    this.http.get('get-my-expenses').subscribe((rep: any) => {
        console.log('get-my-expenses', rep.data.expenses);
        this.updateconf(rep);
      },
      this.manageError);
  }

  save() {
    if (this.config.loading) {
      console.log('already saving');
      return;
    }
    console.log('update expenses...');
    this.config.loading = true;
    this.http.post('save-my-expenses', {
      expenses: this.expenses,
      config: this.config
    })
      .subscribe((rep) => {
          console.log('save-my-expenses', rep);
          this.updateconf(rep);
        },
        (err) => this.manageError(err));
  }

  addExpense() {
    this.expenses.push({
      name: '',
      repeat: 0,
      delay: 0,
      amount: 0,
    });
  }

  init() {
    this.fetchExpenses();
  }


  manageError(error) {
    console.error(error);
    this.config.loading = false;
  }

  updateCanevas() {
    const dataPoints = this.graphPointsPrevision;
    // let chartContainer = new CanvasJS.Chart('simulationPrevision', {
    //   title: {
    //     text: 'Euros disponibles dans le temps'
    //   },
    //   // animationEnabled: true,
    //   data: [
    //     {
    //       // Change type to 'doughnut', 'line', 'splineArea', etc.
    //       type: 'splineArea',
    //       dataPoints: dataPoints
    //     }
    //   ]
    // });
    // chartContainer.render();
  }

}

