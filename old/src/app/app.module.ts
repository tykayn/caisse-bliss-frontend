import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PaiementResumeComponent} from './paiement-resume/paiement-resume.component';
import {ProductListComponent} from './product-list/product-list.component';
import {DashboardOptionsComponent} from './dashboard-options/dashboard-options.component';
import {SellPauseComponent} from './sell-pause/sell-pause.component';
import {ClientInfoComponent} from './client-info/client-info.component';
import {PrevisionnelComponent} from './previsionnel/previsionnel.component';
import {PlanningComponent} from './previsionnel/planning/planning.component';
import {ExpensesComponent} from './previsionnel/expenses/expenses.component';
import {ConfigComponent} from './previsionnel/config/config.component';
import {StatsComponent} from './previsionnel/stats/stats.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        PaiementResumeComponent,
        ProductListComponent,
        DashboardOptionsComponent,
        SellPauseComponent,
        ClientInfoComponent,
        PrevisionnelComponent,
        PlanningComponent,
        ExpensesComponent,
        ConfigComponent,
        StatsComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
    ],
    providers: [],
    exports: [
        ProductListComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
