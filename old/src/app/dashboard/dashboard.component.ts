import {Component, OnInit} from '@angular/core';
import {FetcherService} from '../fetcher.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    constructor(private fetcherService: FetcherService) {
    }

    ngOnInit() {
    }

}
