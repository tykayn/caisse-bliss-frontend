import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PognonConfComponent } from './pognon-conf.component';

describe('PognonConfComponent', () => {
  let component: PognonConfComponent;
  let fixture: ComponentFixture<PognonConfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PognonConfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PognonConfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
