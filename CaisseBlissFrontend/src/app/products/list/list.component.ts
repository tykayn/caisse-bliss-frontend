import { Component } from '@angular/core';
import {NgIf} from '@angular/common';
import {ProductComponent} from '../product/product.component';

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [
    NgIf,
    ProductComponent
  ],
  templateUrl: './list.component.html',
  styleUrl: './list.component.scss'
})
export class ListComponent {
  products:any = [];
}
