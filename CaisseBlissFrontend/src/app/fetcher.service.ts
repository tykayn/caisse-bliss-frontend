import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observer} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetcherService {

  baseUrl: string = 'http://localhost:8000/api/';

  constructor(private httpClient: HttpClient) {
  }

  private products: any[] = [];

  get_products(): any {
    // todo filter by user
    this.httpClient
      .get(this.baseUrl + 'products')
      .subscribe((res: any): any => {
        this.products = res["hydra:member"];
        console.log('res', this.products);
        return res
      });
  }
}
