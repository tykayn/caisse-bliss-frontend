import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellPauseComponent } from './sell-pause.component';

describe('SellPauseComponent', () => {
  let component: SellPauseComponent;
  let fixture: ComponentFixture<SellPauseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SellPauseComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SellPauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
