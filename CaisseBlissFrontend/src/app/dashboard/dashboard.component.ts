import {Component} from '@angular/core';
import {FetcherService} from '../fetcher.service';
import {CommonModule} from '@angular/common';
import {DashboardOptionsComponent} from '../dashboard-options/dashboard-options.component';
import {PognonConfComponent} from '../pognon-conf/pognon-conf.component';
import {SellPauseComponent} from '../sell-pause/sell-pause.component';
import {PaiementResumeComponent} from '../paiement-resume/paiement-resume.component';
import {ListComponent} from '../products/list/list.component';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [CommonModule, DashboardOptionsComponent, PognonConfComponent, SellPauseComponent, PaiementResumeComponent, ListComponent],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent {
  products: any[] = [];

  constructor(private fetcher: FetcherService) {
    this.products = this.fetcher.get_products();
  }


}
