import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrevisionnelComponent } from './previsionnel.component';

describe('PrevisionnelComponent', () => {
  let component: PrevisionnelComponent;
  let fixture: ComponentFixture<PrevisionnelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PrevisionnelComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrevisionnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
